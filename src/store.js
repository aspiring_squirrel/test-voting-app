import React from 'react';

const StoreContext = React.createContext();

const createStore = (WrappedComponent) => {
  return class extends React.Component {
    constructor(props) {
      super(props);
      this.state = { storage: {} };
    }

    // update our state
    updateState = (state) => {
      this.setState({ storage: state });
    };

    /*
     need to mount this as early as possible, to pass through other components
     */
    componentWillMount() {
      const values = {
        get: (key, defaultValue) => {
          let value = this.state.storage[key];
          if (value === undefined) {
            value = defaultValue;
          }
          return value;
        },
        set: (key, value) => {
          const { storage } = this.state;
          storage[key] = value;
          return this.updateState(storage);
        },
        remove: (key) => {
          const { storage } = this.state;
          delete storage[key];
          return this.updateState(storage);
        },
        getState: () => {
          return { ...this.state.store };
        }
      };
      const store = { ...values };
      this.setState({ store });
    }
    render() {
      return (
        <StoreContext.Provider value={{ store: this.state.store }}>
          <WrappedComponent {...this.props} />
        </StoreContext.Provider>
      );
    }
  };
};

const withStore = (WrappedComponent) => {
  return class extends React.Component {
    render() {
      const keys = [];
      return (
        <StoreContext.Consumer>
          {(context) => {
            const { store } = context;
            const storeData = {};
            keys.forEach((key) => {
              storeData[key] = store.get(key);
            });
            return (
              <WrappedComponent store={store} {...this.props} {...storeData} />
            );
          }}
        </StoreContext.Consumer>
      );
    }
  };
};

export { createStore, withStore };
