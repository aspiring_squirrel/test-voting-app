import React, { Component } from 'react';
import { PieChart, Pie } from 'recharts';
import PropTypes from 'prop-types';

class PieComponent extends Component {
  render() {
    const { squirtle, charmander } = this.props;
    const data = [
      { name: 'squirtle', value: squirtle },
      { name: 'charmander', value: charmander }
    ];
    return (
      <PieChart width={400} height={400}>
        <Pie
          dataKey="value"
          startAngle={360}
          endAngle={0}
          data={data}
          cx={200}
          cy={200}
          outerRadius={120}
          fill="#9BA0A4"
          label
        />
      </PieChart>
    );
  }
}

PieComponent.propTypes = {
  squirtle: PropTypes.number,
  charmander: PropTypes.number
};

export default PieComponent;
