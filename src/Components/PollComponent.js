import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { withStore } from '../store';
import PieComponent from './PieComponent';

import { squirtlePic, charmanderPic } from './images';

class PollComponent extends Component {
  constructor(props) {
    super(props);
    this.handleSquirtleVote = this.handleSquirtleVote.bind(this);
    this.handleCharmanderVote = this.handleCharmanderVote.bind(this);
  }

  handleSquirtleVote() {
    const { store } = this.props;
    const squirtleVotes = store.get('squirtleVotes', 0) + 1;
    store.set('squirtleVotes', squirtleVotes);
    // console.log(store.get('squirtleVotes'));
  }

  handleCharmanderVote() {
    const { store } = this.props;
    const charmanderVotes = store.get('charmanderVotes', 0) + 1;
    store.set('charmanderVotes', charmanderVotes);
  }

  render() {
    return (
      <div className="container">
        <h1 className="text-center">Which starter is best?</h1>
        <div className="row">
          <div className="col-sm">
            <img src={squirtlePic} width={300} alt="squirtle" />
            <p>
              votes for Squirtle: {this.props.store.get('squirtleVotes') || 0}
            </p>
            <button className="btn btn-light" onClick={this.handleSquirtleVote}>
              Vote for Squirtle
            </button>
          </div>
          <div className="col-sm">
            <PieComponent
              squirtle={this.props.store.get('squirtleVotes')}
              charmander={this.props.store.get('charmanderVotes')}
            />
          </div>
          <div className="col-sm">
            <div className="float-right">
              <img src={charmanderPic} width={300} alt="charmander" />
              <p>
                votes for Charmander:{' '}
                {this.props.store.get('charmanderVotes') || 0}
              </p>
              <button
                className="btn btn-light"
                onClick={this.handleCharmanderVote}
              >
                Vote for Squirtle
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

PollComponent.propTypes = {
  store: PropTypes.object
};

export default withStore(PollComponent);
