import React from 'react';

import './App.css';
import PollComponent from './Components/PollComponent';

import { createStore } from './store';

function App() {
  return (
    <div>
      <div className="jumbotron">
        <h1 className="display-4">Voting App</h1>
        <p className="lead">
          Create a React application that implements voting for two candidates
          (of your choice) with the display of the ratios of votes in a Pie
          Chart
        </p>
      </div>
      <PollComponent />
    </div>
  );
}

export default createStore(App);
